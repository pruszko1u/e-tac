﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using TouchScript.Gestures.TransformGestures;
using TouchScript.Gestures;
using System;

public class Organizer : MonoBehaviour {

    public Canvas canvas;

    public GameObject[] pages;
    public Image background;
    public int currentPage = 0;

    private double pageHeight;
    private double pageWidth;

    public int pageCount
    {
        get { return pages.Length; }
    }

    void Start () {
        this.pageWidth = GetComponent<RectTransform>().rect.width / 2.5;
        this.pageHeight = GetComponent<RectTransform>().rect.width / 1.5;

        this.UpdatePages();
    }
	
	void Update () {
        this.pageWidth = GetComponent<RectTransform>().rect.width / 2.5;
        this.pageHeight = GetComponent<RectTransform>().rect.width / 1.5;


        this.UpdatePages();
    }

    private void OnEnable()
    {
        // subscribe to gesture's Tapped event
        GetComponent<FlickGesture>().Flicked += OnFlick;

    }
    private void OnDisable()
    {
        GetComponent<FlickGesture>().Flicked -= OnFlick;
    }

    private void OnFlick(object sender, EventArgs e)
    {
        var gesture = sender as FlickGesture;

        if (gesture.ScreenFlickVector.normalized.x < 0)
        {
            if (this.currentPage + 2 < this.pageCount)
                this.currentPage += 2;
            else if (this.currentPage + 1 < this.pageCount)
                this.currentPage++;
        } else
        {
            if (this.currentPage - 2 >= 0)
                this.currentPage -= 2;
            else if (this.currentPage - 1 >= 0)
                this.currentPage--;
        }

    }

    void UpdatePages()
    {
        for (int i = 0 ; i < pageCount; i++)
        {
            pages[i].GetComponent<RectTransform>().sizeDelta = new Vector2((float)this.pageWidth, (float)this.pageHeight);
            pages[i].GetComponent<TransformGesture>().Type = TransformGesture.TransformType.None;

            if (i == currentPage)
            {
                pages[i].GetComponent<CanvasRenderer>().SetAlpha(1);
                pages[i].GetComponent<RectTransform>().localPosition = new Vector3(-70, 0, 0);
            }
            else if (i == currentPage + 1)
            {
                pages[i].GetComponent<CanvasRenderer>().SetAlpha(1);
                pages[i].GetComponent<RectTransform>().localPosition = new Vector3(70, 0, 0);
            }
            else
            {
                pages[i].GetComponent<CanvasRenderer>().SetAlpha(0);
            }

        }
    }
}
